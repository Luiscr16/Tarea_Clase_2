package visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class Main extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textField1;
    private JTextField textField2;
    private JTable table1;
    private JButton eliminarButton;
    private JTextField textField3;
    private DefaultTableModel model;

    public Main() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        model=new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Dirección");
        model.addColumn("Edad");
        table1.setModel(model);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });
    }

    private void onOK() {
        // add your code here
        //dispose();
        String nombreVar=textField1.getText();
        String direccionVar=textField3.getText();
        int edadVar=Integer.parseInt(textField2.getText());

        textField1.setText("");
        textField2.setText("");
        textField3.setText("");

        JOptionPane.showConfirmDialog(getParent(), "El nombre ingresado es: "+nombreVar+" la dirección es: "+direccionVar+" y la edad es: "+edadVar+"años");
        model.addRow(new Object[]{nombreVar,direccionVar,edadVar});

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Main dialog = new Main();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
