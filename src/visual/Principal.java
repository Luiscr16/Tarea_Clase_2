package visual;

import javax.swing.*;
import java.awt.event.*;

public class Principal extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox comboBox1;


    public Principal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        //dispose();
        String valorComboBox="";
        valorComboBox=comboBox1.getSelectedItem().toString();
        JOptionPane.showMessageDialog(this, "usted selecciono la opción "+valorComboBox+" Gracias");
        if (valorComboBox=="Ingresar Alumnos"){
        Main vermenu=new Main();
        vermenu.pack();
        vermenu.setVisible(true);
        }
        else
        if (valorComboBox=="Ingresar Cursos"){
            Cursos vermenu=new Cursos();
            vermenu.pack();
            vermenu.setVisible(true);
        }
        else
        if (valorComboBox=="Ingresar Grados"){
            Grad vermenu=new Grad();
            vermenu.pack();
            vermenu.setVisible(true);
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();

    }

    public static void main(String[] args) {
        Principal dialog = new Principal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
