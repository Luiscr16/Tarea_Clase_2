package Clases;

/**
 * Created by Luis on 6/07/2017.
 */
public class Grados {
    private String nombre;

    public Grados(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
