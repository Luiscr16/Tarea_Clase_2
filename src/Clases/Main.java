package Clases;

import java.util.Scanner;

/**
 * Created by Luis on 6/07/2017.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Control control = null;
        boolean salir = false;
        int c;
        int opcion;
        Scanner sn = new Scanner(System.in);

        while (!salir) {
            System.out.println("1. Agregar Alumnos");
            System.out.println("2. Mostrar Alumnos");
            System.out.println("3. Salir");
            System.out.println("4. Agregar Cursos");
            System.out.println("5. Mostrar Cursos");

            System.out.println("Sistema de control de Alumnos");
            System.out.println("Sleccione una de las opciones");
            opcion=sn.nextInt();
            switch (opcion){
                case 1:
                    System.out.println("ingrese la cantidad de Alumnos a su cargo");
                    c = sn.nextInt();
                    control = new Control(c);


                    for (int i = 0; i <= (c - 1); i++) {
                        System.out.println("Ingrese el Nombre: ");
                        Alumno alumno = new Alumno();
                        alumno.setNombre(sn.next());
                        System.out.println("Edad del Alumno");
                        alumno.setEdad(sn.nextInt());
                        control.adicionarAlumno(alumno);
                    }
                    break;
                case 2:
                    Alumno[] listado = control.getListado();
                    for (int i = 0; i < control.getCantReal(); i++) {
                        System.out.println("Alumno: " + (i + 1) + ": " + listado[i].getNombre());

                    }
                    break;
                case 3:
                    salir = true;
                    break;
                case 4:
                    System.out.println("ingrese la cantidad de cursos");
                    c = sn.nextInt();
                    control = new Control(c);
                    for (int i=0; i<=(c-1); i++){
                        Cursos cursos = new Cursos();
                        System.out.println("Nombre del curso:");
                        cursos.setNombre(sn.next());
                        control.adicionarCurso(cursos);
                    }
                    break;
                case 5:
                    int x = 0;
                    while (x < control.getListadoCursos().size()){
                        System.out.println(control.getListadoCursos().get(x).getNombre());
                    x++;
                    }
                    break;
                default:
                    System.out.println("Solo numeros del 1 - 5");

            }


        }
    }
}